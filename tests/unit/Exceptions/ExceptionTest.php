<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 12:07 PM
 */

class ExceptionTest extends \PHPUnit\Framework\TestCase
{

    public function testSystemExceptionRenderShowsDefaultMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\SystemException('foo bar');
        $this->assertNotContains('foo bar', $sut->render());
        $this->assertEquals('foo bar', $sut->getMessage());
    }
    public function testSystemExceptionRenderWithOverrideMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\SystemException('foo bar');
        $this->assertEquals('fiz buz', $sut->render('fiz buz'));
        $this->assertEquals('foo bar', $sut->getMessage());
    }

    public function testAuthExceptionRenderShowsDefaultMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\AuthException('foo bar');
        $this->assertNotContains('foo bar', $sut->render());
        $this->assertEquals('foo bar', $sut->getMessage());
    }
    public function testAuthExceptionRenderWithOverrideMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\AuthException('foo bar');
        $this->assertEquals('fiz buz', $sut->render('fiz buz'));
        $this->assertEquals('foo bar', $sut->getMessage());
    }

    public function testSystemDisplayableExceptionShowsMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\SystemDisplayableException('foo bar');
        $this->assertEquals('foo bar', $sut->render());
    }

    public function testSystemDisplayableExceptionWithOverrideMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\SystemDisplayableException('foo bar');
        $this->assertEquals('fiz buz', $sut->render('fiz buz'));
    }

    public function testAuthDisplayableExceptionShowsMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\AuthDisplayableException('foo bar');
        $this->assertEquals('foo bar', $sut->render());
    }

    public function testAuthDisplayableExceptionWithOverrideMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\AuthDisplayableException('foo bar');
        $this->assertEquals('fiz buz', $sut->render('fiz buz'));
    }

    public function testAuthDisplayableExceptionShowsDisplayable()
    {
        $sut = new \Smorken\Auth\Exceptions\AuthDisplayableException('foo bar', 'show me');
        $this->assertEquals('show me', $sut->render());
    }

    public function testAuthDisplayableExceptionDisplayableWithOverrideMessage()
    {
        $sut = new \Smorken\Auth\Exceptions\AuthDisplayableException('foo bar', 'show me');
        $this->assertEquals('fiz buz', $sut->render('fiz buz'));
    }

    public function testDisplayableIsThrowable()
    {
        $this->expectException(\Smorken\Auth\Exceptions\BaseDisplayableException::class);
        throw new \Smorken\Auth\Exceptions\AuthDisplayableException('foo bar', 'show me');
    }
}
