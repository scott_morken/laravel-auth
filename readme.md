## Common auth extensions Laravel 5+ for use with smorken/auth-user and smorken/ldap-auth

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
