<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 11:51 AM
 */

namespace Smorken\Auth\Exceptions;

abstract class BaseDisplayableException extends \Exception implements \Smorken\Auth\Contracts\Exception
{

    protected $displayable;

    public function __construct($message, $displayable = null, $code = 0, \Exception $previous = null)
    {
        $this->displayable = $displayable ?: $message;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param null $override_msg
     * @return string
     */
    public function render($override_msg = null)
    {
        if ($override_msg !== null) {
            return $override_msg;
        }
        return $this->displayable;
    }
}
