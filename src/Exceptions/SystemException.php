<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 11:53 AM
 */

namespace Smorken\Auth\Exceptions;

class SystemException extends BaseException
{

    protected $default_msg = 'There was a system error.  Please try your request again.';
}
