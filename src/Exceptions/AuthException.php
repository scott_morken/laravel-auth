<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 11:54 AM
 */

namespace Smorken\Auth\Exceptions;

class AuthException extends BaseException
{

    protected $default_msg = 'There was an authentication error. Please try your request again.';
}
