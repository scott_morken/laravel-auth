<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 12:02 PM
 */

namespace Smorken\Auth\Exceptions;

abstract class BaseException extends \Exception implements \Smorken\Auth\Contracts\Exception
{

    protected $default_msg = 'There has been an error in the application.';

    /**
     * @param null $override_msg
     * @return string
     */
    public function render($override_msg = null)
    {
        if ($override_msg !== null) {
            return $override_msg;
        }
        return $this->default_msg;
    }
}
