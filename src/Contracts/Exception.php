<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 12:01 PM
 */

namespace Smorken\Auth\Contracts;

interface Exception
{

    /**
     * @param null $override_msg
     * @return string
     */
    public function render($override_msg = null);
}
